<?php

use konstr_prog\lab_3\task1\FileLoggerAdapter;
use konstr_prog\lab_3\task1\FileWriter;

include_once('FileWriter.php');
include_once('Logger.php');
include_once('FileLoggerAdapter.php');
function main()
{
    $path = "C:\\wamp64\\domains\\konstr_prog\\lab_3\\task1\\adapter.txt";
    $fileWriter = new FileWriter($path);
    $fileLoggerAdapter = new FileLoggerAdapter($fileWriter);

    $fileLoggerAdapter->Log();
    $fileLoggerAdapter->Error();
    $fileLoggerAdapter->Warn();

}

main();
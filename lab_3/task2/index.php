<?php


include_once ('Hero.php');
include_once ('Warrior.php');
include_once ('Mage.php');
include_once ('Palladin.php');
include_once ('Equipment.php');
include_once ('BasicEquipment.php');
include_once ('EquipmentDecorator.php');
include_once ('ClothesDecorator.php');
include_once ('WeaponDecorator.php');
include_once ('ArtifactDecorator.php');

//spl_autoload_register(static function ($className){
//    $path =str_replace('\\','/',$className . ".php");
//    if (file_exists($path))
//        include_once($path);
//    echo $path;
//});

use konstr_prog\lab_3\task2\Mage;
use konstr_prog\lab_3\task2\BasicEquipment;
use konstr_prog\lab_3\task2\ClothesDecorator;
use konstr_prog\lab_3\task2\WeaponDecorator;
use konstr_prog\lab_3\task2\Warrior;
use konstr_prog\lab_3\task2\Palladin;
use konstr_prog\lab_3\task2\ArtifactDecorator;


$warrior = new Warrior();
$mage = new Mage();
$paladin = new Palladin();

$basicEquipment = new BasicEquipment();
$equipmentForWarrior = new ClothesDecorator(new WeaponDecorator($basicEquipment));
$equipmentForMage = new WeaponDecorator(new ArtifactDecorator($basicEquipment));
$equipmentForPaladin = new ClothesDecorator(new ArtifactDecorator($basicEquipment));

echo $warrior->getDescription() . " with " . $equipmentForWarrior->getDescription() . "\n";
echo $mage->getDescription() . " with " . $equipmentForMage->getDescription() . "\n";
echo $paladin->getDescription() . " with " . $equipmentForPaladin->getDescription() . "\n";
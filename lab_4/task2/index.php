<?php
include_once ('Aircraft.php');
include_once ('CommandCenter.php');
include_once ('Runway.php');

use konstr_prog\lab_4\task2\Aircraft;
use konstr_prog\lab_4\task2\CommandCenter;
use konstr_prog\lab_4\task2\Runway;

$commandCentre = new CommandCenter();
$runway1 = new Runway();
$runway2 = new Runway();

$commandCentre->addRunway($runway1);
$commandCentre->addRunway($runway2);

$aircraft1 = new Aircraft("Boeing 747", $commandCentre);
$aircraft2 = new Aircraft("Airbus A380", $commandCentre);

$commandCentre->addAircraft($aircraft1);
$commandCentre->addAircraft($aircraft2);

$aircraft1->land();
$aircraft2->land();
$aircraft1->takeOff();
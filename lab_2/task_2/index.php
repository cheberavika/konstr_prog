<?php
require_once ('AbstractFabric.php');

$iproneFactory = new IProneFactory();
$kiaomiFactory = new KiaomiFactory();
$balaxyFactory = new BalaxyFactory();

$laptop1 = $iproneFactory->createLaptop();
$laptop2 = $kiaomiFactory->createLaptop();
$laptop3 = $balaxyFactory->createLaptop();

$smartphone1 = $iproneFactory->createSmartphone();
$smartphone2 = $kiaomiFactory->createSmartphone();
$smartphone3 = $balaxyFactory->createSmartphone();

echo $laptop1->getModel() . "<br>";
echo $laptop2->getModel() . "<br>";
echo $laptop3->getModel() . "<br>";

echo $smartphone1->getModel() . "<br>";
echo $smartphone2->getModel() . "<br>";
echo $smartphone3->getModel() . "<br>";
<?php
require_once "Character.php";
require_once "Builder.php";
require_once "HeroBuilder.php";
require_once "EnemyBuilder.php";

use konstr_prog\lab_2\task_5\HeroBuilder;
use konstr_prog\lab_2\task_5\EnemyBuilder;

$heroBuilder = new HeroBuilder();
$hero = $heroBuilder->setHeight(190)
    ->setBuild("Athletic")
    ->setHairColor("Brown")
    ->setEyeColor("Green")
    ->setClothing("Armor")
    ->setInventory(["Sword", "Shield"])
    ->createCharacter();

$enemyBuilder = new EnemyBuilder();
$enemy = $enemyBuilder->setHeight(150)
    ->setBuild("Skinny")
    ->setHairColor("White")
    ->setEyeColor("Blue")
    ->setClothing("Dark Robe")
    ->setInventory(["Magic Staff"])
    ->createCharacter();

echo "Hero Character: \n";
var_dump($hero);
echo "\nEnemy Character: \n";
var_dump($enemy);